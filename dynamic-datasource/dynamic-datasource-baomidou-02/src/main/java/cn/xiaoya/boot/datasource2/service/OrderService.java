package cn.xiaoya.boot.datasource2.service;

import cn.xiaoya.boot.datasource2.constants.DbConstants;
import cn.xiaoya.boot.datasource2.dataobject.OrderDO;
import cn.xiaoya.boot.datasource2.mapper.OrderMapper;
import com.baomidou.dynamic.datasource.annotation.DS;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author xieya
 * @date 2021/7/26
 */
@Service
@Slf4j
public class OrderService {
    @Autowired
    private OrderMapper orderMapper;

    @Transactional(rollbackFor = Exception.class)
    @DS(DbConstants.DATASOURCE_MASTER)
    public void add(OrderDO order) {
        // 这里先假模假样的读取一下
        OrderDO orderDO = orderMapper.selectById(5L);
        log.info("order=({})", orderDO);

        // 插入订单
        orderMapper.insert(order);
        log.info("订单id=({}})插入成功", order.getId());
    }

    public OrderDO findById(Long id) {
        return orderMapper.selectById(id);
    }
}
