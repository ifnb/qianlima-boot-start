package cn.xiaoya.boot.datasource2.mapper;

import cn.xiaoya.boot.datasource2.constants.DbConstants;
import cn.xiaoya.boot.datasource2.dataobject.OrderDO;
import com.baomidou.dynamic.datasource.annotation.DS;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * @author xieya
 * @date 2021/7/26
 */
@Repository
public interface OrderMapper {
    /**
     * 根据ID查询订单信息
     * @param id  订单ID
     * @return
     */
    @DS(value = DbConstants.DATASOURCE_SLAVE)
    OrderDO selectById(@Param("id") Long id);

    /**
     * 插入订单信息
     * @param entity 订单信息
     * @return
     */
    @DS(DbConstants.DATASOURCE_MASTER)
    int insert(OrderDO entity);
}
