package cn.xiaoya.boot.datasource2.constants;

/**
 * 数据源通用常量
 *
 * @author xieya
 * @date 2021/7/26
 */
public final class DbConstants {
    public static final String DATASOURCE_ORDERS = "orders";
    public static final String DATASOURCE_USERS = "users";
    public static final String DATASOURCE_SLAVE = "slave";
    public static final String DATASOURCE_MASTER = "master";
}
