package cn.xiaoya.boot.datasource2.service;

import cn.xiaoya.boot.datasource2.Baomidou02App;
import cn.xiaoya.boot.datasource2.dataobject.OrderDO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 注意。使用事务必须指定数据源，否则会报错
 *
 * @author xieya
 * @date 2021/7/26
 */
@SpringBootTest(classes = Baomidou02App.class)
@RunWith(SpringRunner.class)
public class OrderServiceTest {
    @Autowired
    private OrderService orderService;

    @Test
    public void testAdd() {
        OrderDO order = new OrderDO();
        order.setUserId(266L);
        // 主读主写
        orderService.add(order);
    }

    @Test
    public void testFindById() {
        // 从从库中读取
        OrderDO order = orderService.findById(1L);
        System.out.println(order);
    }
}
