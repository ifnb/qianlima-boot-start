package cn.xiaoya.boot.datasource2;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author xieya
 * @date 2021/7/26
 */
@SpringBootTest(classes = Baomidou02App.class)
@RunWith(SpringRunner.class)
public class AppTest {
}
