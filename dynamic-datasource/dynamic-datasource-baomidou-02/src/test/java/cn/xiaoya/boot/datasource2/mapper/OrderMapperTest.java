package cn.xiaoya.boot.datasource2.mapper;

import cn.xiaoya.boot.datasource2.Baomidou02App;
import cn.xiaoya.boot.datasource2.dataobject.OrderDO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author xieya
 * @date 2021/7/26
 */
@SpringBootTest(classes = Baomidou02App.class)
@RunWith(SpringRunner.class)
public class OrderMapperTest {
    @Autowired
    private OrderMapper orderMapper;

    @Test
    public void testSelectById() {
        for (int i = 0; i < 10; i++) {
            // 轮询读取slave1和slave2
            OrderDO order = orderMapper.selectById(1L);
            System.out.println(order);
        }
    }

    @Test
    public void testInsert() {
        OrderDO order = new OrderDO();
        order.setUserId(10L);
        orderMapper.insert(order);
    }
}
