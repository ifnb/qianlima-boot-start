package cn.xiaoya.boot.sharding2.service;

import cn.xiaoya.boot.sharding2.ShardingJdbc2App;
import cn.xiaoya.boot.sharding2.dataobject.OrderDO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author xieya
 * @date 2021/7/28
 */
@SpringBootTest(classes = ShardingJdbc2App.class)
@RunWith(SpringRunner.class)
public class OrderServiceTest {
    @Autowired
    private OrderService orderService;

    @Test
    public void testAdd() {
        OrderDO order = new OrderDO();
        order.setUserId(266L);
        // 主读主写
        orderService.add(order);


    }

    @Test
    public void testFindById() {
        // 从从库中读取
        OrderDO order = orderService.findById(1L);
        System.out.println(order);
    }
}
