package cn.xiaoya.boot.sharding2.mapper;

import cn.xiaoya.boot.sharding2.ShardingJdbc2App;
import cn.xiaoya.boot.sharding2.dataobject.OrderDO;
import org.apache.shardingsphere.api.hint.HintManager;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author xieya
 * @date 2021/7/28
 */
@SpringBootTest(classes = ShardingJdbc2App.class)
@RunWith(SpringRunner.class)
public class OrderMapperTest {
    @Autowired
    private OrderMapper orderMapper;

    @Test
    public void testSelectById() {
        // 从库负载均衡
        for (int i = 0; i < 10; i++) {
            // 轮询读取slave1和slave2
            OrderDO order = orderMapper.selectById(1L);
            System.out.println(order);
        }
    }

    @Test
    public void testInsert() {
        OrderDO order = new OrderDO();
        order.setUserId(99L);
        orderMapper.insert(order);
    }

    @Test
    public void selectMaster() {
        // 从主库中查询
        try (HintManager hintManager = HintManager.getInstance()) {
            // 设置强制访问主库
            hintManager.setMasterRouteOnly();
            // 执行查询
            OrderDO order = orderMapper.selectById(8L);
            System.out.println(order);
        }
    }
}
