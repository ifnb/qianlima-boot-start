package cn.xiaoya.boot.sharding2;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * ShardingJdbc 读写分离
 *
 * @author xieya
 * @date 2021/7/27
 */
@SpringBootApplication
@MapperScan(basePackages = "cn.xiaoya.boot.sharding2.mapper")
@EnableAspectJAutoProxy(exposeProxy = true)
public class ShardingJdbc2App {
    public static void main(String[] args) {
        SpringApplication.run(ShardingJdbc2App.class, args);
    }
}
