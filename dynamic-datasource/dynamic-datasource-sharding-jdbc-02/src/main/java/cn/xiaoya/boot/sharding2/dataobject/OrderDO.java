package cn.xiaoya.boot.sharding2.dataobject;

import lombok.Data;

/**
 * @author xieya
 * @date 2021/7/26
 */
@Data
public class OrderDO {
    private Long id;
    private Long userId;
}
