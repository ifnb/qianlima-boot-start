package cn.xiaoya.boot.sharding2.mapper;


import cn.xiaoya.boot.sharding2.dataobject.OrderDO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * @author xieya
 * @date 2021/7/26
 */
@Repository
public interface OrderMapper {
    /**
     * 根据ID查询订单信息
     *
     * @param id 订单ID
     * @return
     */
    OrderDO selectById(@Param("id") Long id);

    /**
     * 插入订单信息
     *
     * @param entity 订单信息
     * @return
     */
    int insert(OrderDO entity);
}
