package cn.xiaoyacode.sharding.datasource.mapper;

import cn.xiaoyacode.sharding.datasource.dataobject.OrderDO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author xieya
 * @date 2021/7/28
 */
@Repository
public interface OrderMapper {

    OrderDO selectById(@Param("id") Long id);

    List<OrderDO> selectListByUserId(@Param("userId") Integer userId);

    void insert(OrderDO order);

}
