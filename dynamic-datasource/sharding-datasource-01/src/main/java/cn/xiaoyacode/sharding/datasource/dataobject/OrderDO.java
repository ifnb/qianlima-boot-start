package cn.xiaoyacode.sharding.datasource.dataobject;

import lombok.Data;

/**
 * @author xieya
 * @date 2021/7/28
 */
@Data
public class OrderDO {
    /**
     * 订单编号
     */
    private Long id;
    /**
     * 用户编号
     */
    private Integer userId;
}
