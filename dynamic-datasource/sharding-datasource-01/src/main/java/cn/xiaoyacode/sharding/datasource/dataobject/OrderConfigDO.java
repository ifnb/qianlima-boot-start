package cn.xiaoyacode.sharding.datasource.dataobject;

import lombok.Data;

/**
 * @author  xieya
 * @date  2021/7/28
 */
@Data
public class OrderConfigDO {
    /**
     * 编号
     */
    private Integer id;
    /**
     * 支付超时时间
     *
     * 单位：分钟
     */
    private Integer payTimeout;
}
