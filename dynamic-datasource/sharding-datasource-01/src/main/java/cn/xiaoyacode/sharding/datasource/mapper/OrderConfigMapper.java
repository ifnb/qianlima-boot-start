package cn.xiaoyacode.sharding.datasource.mapper;

import cn.xiaoyacode.sharding.datasource.dataobject.OrderConfigDO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * @author xieya
 * @date 2021/7/28
 */
@Repository
public interface OrderConfigMapper {

    OrderConfigDO selectById(@Param("id") Integer id);

}
