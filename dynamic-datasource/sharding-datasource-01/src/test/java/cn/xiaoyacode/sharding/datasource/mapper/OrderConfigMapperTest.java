package cn.xiaoyacode.sharding.datasource.mapper;

import cn.xiaoyacode.sharding.datasource.ShardingApp;
import cn.xiaoyacode.sharding.datasource.dataobject.OrderConfigDO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author xieya
 * @date 2021/7/28
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ShardingApp.class)
public class OrderConfigMapperTest {
    @Autowired
    private OrderConfigMapper orderConfigMapper;

    @Test
    public void testSelectById() {
        // 不需要分片规则的表  Logic SQL 和 Actual SQL 也只需要执行1次
        OrderConfigDO orderConfig = orderConfigMapper.selectById(1);
        System.out.println(orderConfig);
    }
}
