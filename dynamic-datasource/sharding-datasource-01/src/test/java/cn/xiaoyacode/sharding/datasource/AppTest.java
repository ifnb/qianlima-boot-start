package cn.xiaoyacode.sharding.datasource;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author xieya
 * @date 2021/7/28
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ShardingApp.class)
public class AppTest {
}
