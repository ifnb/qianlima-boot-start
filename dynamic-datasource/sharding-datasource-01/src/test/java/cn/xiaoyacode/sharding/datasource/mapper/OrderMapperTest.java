package cn.xiaoyacode.sharding.datasource.mapper;

import cn.xiaoyacode.sharding.datasource.ShardingApp;
import cn.xiaoyacode.sharding.datasource.dataobject.OrderDO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @author xieya
 * @date 2021/7/28
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ShardingApp.class)
public class OrderMapperTest {
    @Autowired
    OrderMapper orderMapper;

    @Test
    public void testSelectById() {
        // 非分片字段，默认会轮询2个库的所有表：Logic SQL 1次，Actual SQL所有库的所有分片表
        OrderDO order = orderMapper.selectById(627184641664614400L);
        System.out.println(order);
    }

    @Test
    public void testSelectListByUserId() {
        // 分片字段 Logic SQL 和 Actual SQL 都只执行1次
        List<OrderDO> orders = orderMapper.selectListByUserId(1);
        System.out.println(orders);
    }

    @Test
    public void testInsert() {
        OrderDO order = new OrderDO();
        // init 10条数据
        for (int i = 1; i <= 10; i++) {
            order.setUserId(i);
            orderMapper.insert(order);
        }
    }
}
