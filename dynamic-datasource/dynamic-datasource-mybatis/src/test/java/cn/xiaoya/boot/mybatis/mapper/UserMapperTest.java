package cn.xiaoya.boot.mybatis.mapper;

import cn.xiaoya.boot.mybatis.MybatisApp;
import cn.xiaoya.boot.mybatis.dataobject.UserDO;
import cn.xiaoya.boot.mybatis.mapper.users.UserMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author xieya
 * @date 2021/7/26
 */
@SpringBootTest(classes = MybatisApp.class)
@RunWith(SpringRunner.class)
@Slf4j
public class UserMapperTest {
    @Autowired
    private UserMapper userMapper;

    @Test
    public void testSelectById() {
        UserDO user = userMapper.selectById(1L);
        log.info("user=({})", user);
    }
}
