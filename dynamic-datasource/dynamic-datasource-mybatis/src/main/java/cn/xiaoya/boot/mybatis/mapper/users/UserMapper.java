package cn.xiaoya.boot.mybatis.mapper.users;

import cn.xiaoya.boot.mybatis.dataobject.UserDO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * @author xieya
 * @date 2021/7/26
 */
@Repository
public interface UserMapper {
    /**
     * 根据ID查询用户信息
     * @param id  用户ID
     * @return
     */
    UserDO selectById(@Param("id") Long id);
}
