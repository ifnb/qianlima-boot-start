package cn.xiaoya.boot.mybatis.dataobject;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author xieya
 * @date 2021/7/26
 */
@Data
public class UserDO {
    private Long id;
    private String username;
    private String password;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
}
