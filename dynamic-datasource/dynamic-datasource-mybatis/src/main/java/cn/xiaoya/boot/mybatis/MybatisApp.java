package cn.xiaoya.boot.mybatis;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * mybatis多数据源
 * EnableAspectJAutoProxy(exposeProxy = true) see http://www.voidcn.com/
 * @author xieya
 * @date 2021/7/26
 */
@SpringBootApplication
@EnableAspectJAutoProxy(exposeProxy = true)
public class MybatisApp {
    public static void main(String[] args) {
        SpringApplication.run(MybatisApp.class, args);
    }
}
