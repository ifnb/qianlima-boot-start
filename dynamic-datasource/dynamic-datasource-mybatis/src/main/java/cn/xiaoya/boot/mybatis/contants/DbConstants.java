package cn.xiaoya.boot.mybatis.contants;

/**
 * @author xieya
 * @date 2021/7/26
 */
public final class DbConstants {
    /**
     * 事务管理器 - 订单库
     */
    public static final String TX_MANAGER_ORDERS = "ordersTransactionManager";

    /**
     * 事务管理器 - 用户库
     */
    public static final String TX_MANAGER_USERS = "usersTransactionManager";
}
