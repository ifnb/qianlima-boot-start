package cn.xiaoya.boot.jdbctemplate.dao;

import cn.xiaoya.boot.jdbctemplate.JdbcTemplateApp;
import cn.xiaoya.boot.jdbctemplate.dataobject.OrderDO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author xieya
 * @date 2021/7/27
 */
@SpringBootTest(classes = JdbcTemplateApp.class)
@RunWith(SpringRunner.class)
public class OrderDaoTest {
    @Autowired
    OrderDao orderDao;

    @Test
    public void selectByIdTest() {
        OrderDO orderDO = orderDao.selectById(1L);
        System.out.println(orderDO);
    }
}
