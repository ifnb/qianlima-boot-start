package cn.xiaoya.boot.jdbctemplate.service;

import cn.xiaoya.boot.jdbctemplate.JdbcTemplateApp;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author xieya
 * @date 2021/7/27
 */
@SpringBootTest(classes = JdbcTemplateApp.class)
@RunWith(SpringRunner.class)
public class OrderServiceTest {
    @Autowired
    OrderService orderService;

    @Test
    public void test1() {
        orderService.method01();
    }

    @Test
    public void test2() {
       orderService.method02();
    }

    @Test
    public void test3() {
      orderService.method03();
    }

    @Test
    public void test4() {
       orderService.method04();
    }

    @Test
    public void test5() {
        orderService.method05();
    }
}
