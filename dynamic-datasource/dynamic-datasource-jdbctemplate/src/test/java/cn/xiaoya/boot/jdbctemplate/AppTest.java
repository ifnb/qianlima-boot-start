package cn.xiaoya.boot.jdbctemplate;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author xieya
 * @date 2021/7/27
 */
@SpringBootTest(classes = JdbcTemplateApp.class)
@RunWith(SpringRunner.class)
public class AppTest {
}
