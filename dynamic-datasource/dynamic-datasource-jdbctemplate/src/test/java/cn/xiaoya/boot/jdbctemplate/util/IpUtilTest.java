package cn.xiaoya.boot.jdbctemplate.util;

import cn.xiaoya.boot.jdbctemplate.JdbcTemplateApp;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author xieya
 * @date 2021/7/27
 */
@SpringBootTest(classes = JdbcTemplateApp.class)
@RunWith(SpringRunner.class)
public class IpUtilTest {
    @Test
    public void ip2LongTest() throws Exception {
        long ipLong = IpUtil.ip2Long("192.168.33.22");
        System.out.println(ipLong);
    }

    @Test
    public void long2IpTest(){
        String ip = IpUtil.long2Ip(3232243990L);
        System.out.println(ip);
    }
}
