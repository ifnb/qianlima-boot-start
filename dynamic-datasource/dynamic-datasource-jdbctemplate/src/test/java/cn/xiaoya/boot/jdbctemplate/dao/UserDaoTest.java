package cn.xiaoya.boot.jdbctemplate.dao;

import cn.xiaoya.boot.jdbctemplate.JdbcTemplateApp;
import cn.xiaoya.boot.jdbctemplate.dataobject.UserDO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author xieya
 * @date 2021/7/27
 */
@SpringBootTest(classes = JdbcTemplateApp.class)
@RunWith(SpringRunner.class)
public class UserDaoTest {
    @Autowired
    UserDao userDao;

    @Test
    public void selectByIdTest(){
        UserDO userDO = userDao.selectById(1L);
        System.out.println(userDO);
    }
}
