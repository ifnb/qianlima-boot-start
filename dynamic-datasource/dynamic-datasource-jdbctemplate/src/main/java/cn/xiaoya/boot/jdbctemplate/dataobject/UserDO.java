package cn.xiaoya.boot.jdbctemplate.dataobject;

import lombok.Data;

import java.util.Date;

/**
 * 用户 DO
 * @author xieya
 * @date 2021/7/26
 */
@Data
public class UserDO {

    /**
     * 用户编号
     */
    private Long id;
    /**
     * 账号
     */
    private String username;
    private String password;
    private Date createTime;
}
