package cn.xiaoya.boot.jdbctemplate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * @author xieya
 * @date 2021/7/27
 */
@SpringBootApplication
@EnableAspectJAutoProxy(exposeProxy = true)
public class JdbcTemplateApp {
    public static void main(String[] args) {
        SpringApplication.run(JdbcTemplateApp.class, args);
    }
}
