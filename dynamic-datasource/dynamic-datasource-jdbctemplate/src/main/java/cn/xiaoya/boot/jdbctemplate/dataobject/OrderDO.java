package cn.xiaoya.boot.jdbctemplate.dataobject;


import lombok.Data;

/**
 * 订单 DO
 * @author xieya
 * @date 2021/7/26
 */
@Data
public class OrderDO {

    /**
     * 订单编号
     */
    private Long id;
    /**
     * 用户编号
     */
    private Long userId;

}
