package cn.xiaoya.boot.jdbctemplate.service;

import cn.xiaoya.boot.jdbctemplate.constants.DbConstants;
import cn.xiaoya.boot.jdbctemplate.dao.OrderDao;
import cn.xiaoya.boot.jdbctemplate.dao.UserDao;
import cn.xiaoya.boot.jdbctemplate.dataobject.OrderDO;
import cn.xiaoya.boot.jdbctemplate.dataobject.UserDO;
import org.springframework.aop.framework.AopContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author xieya
 * @date 2021/7/27
 */
@Service
public class OrderService {

    @Autowired
    private OrderDao orderDao;
    @Autowired
    private UserDao userDao;

    private OrderService self() {
        return (OrderService) AopContext.currentProxy();
    }

    public void method01() {
        // 查询订单
        OrderDO order = orderDao.selectById(1L);
        System.out.println(order);
        // 查询用户
        UserDO user = userDao.selectById(1L);
        System.out.println(user);
    }

    /**
     * 报错，找不到事务管理器
     */
    @Transactional(rollbackFor = Exception.class)
    public void method02() {
        // 查询订单
        OrderDO order = orderDao.selectById(1L);
        System.out.println(order);
        // 查询用户
        UserDO user = userDao.selectById(1L);
        System.out.println(user);
    }

    public void method03() {
        // 查询订单
        self().method031();
        // 查询用户
        self().method032();
    }

    @Transactional(transactionManager = DbConstants.TX_MANAGER_ORDERS, rollbackFor = Exception.class)
    public void method031() {
        OrderDO order = orderDao.selectById(1L);
        System.out.println(order);
    }

    @Transactional(transactionManager = DbConstants.TX_MANAGER_USERS, rollbackFor = Exception.class)
    public void method032() {
        UserDO user = userDao.selectById(1L);
        System.out.println(user);
    }

    public void method04() {
        this.method02();
    }

    @Transactional(transactionManager = DbConstants.TX_MANAGER_ORDERS, rollbackFor = Exception.class)
    public void method05() {
        // 查询订单
        OrderDO order = orderDao.selectById(1L);
        System.out.println(order);
        // 查询用户
        self().method052();
    }

    @Transactional(transactionManager = DbConstants.TX_MANAGER_USERS,
            propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public void method052() {
        UserDO user = userDao.selectById(1L);
        System.out.println(user);
    }

}
