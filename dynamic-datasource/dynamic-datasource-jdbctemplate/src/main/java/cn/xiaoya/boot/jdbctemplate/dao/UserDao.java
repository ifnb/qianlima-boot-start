package cn.xiaoya.boot.jdbctemplate.dao;

import cn.xiaoya.boot.jdbctemplate.constants.DbConstants;
import cn.xiaoya.boot.jdbctemplate.dataobject.UserDO;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
 * @author xieya
 * @date 2021/7/27
 */
@Repository
public class UserDao {

    @Resource(name = DbConstants.JDBC_TEMPLATE_USERS)
    private JdbcTemplate template;

    public UserDO selectById(Long id) {
        final String sql = "SELECT id, username, password, create_time FROM users WHERE id = ?";
        return template.queryForObject(sql,
                // 结果转换成对应的对象
                new BeanPropertyRowMapper<>(UserDO.class), id);
    }

}
