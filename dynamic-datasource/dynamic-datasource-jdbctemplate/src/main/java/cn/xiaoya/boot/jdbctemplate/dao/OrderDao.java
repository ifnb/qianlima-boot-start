package cn.xiaoya.boot.jdbctemplate.dao;

import cn.xiaoya.boot.jdbctemplate.constants.DbConstants;
import cn.xiaoya.boot.jdbctemplate.dataobject.OrderDO;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
 * @author xieya
 * @date 2021/7/27
 */
@Repository
public class OrderDao {

    @Resource(name = DbConstants.JDBC_TEMPLATE_ORDERS)
    private JdbcTemplate template;

    public OrderDO selectById(Long id) {
        final String sql = "SELECT id, user_id FROM orders WHERE id = ?";
        return template.queryForObject(sql,
                // 结果转换成对应的对象
                new BeanPropertyRowMapper<>(OrderDO.class), id);
    }

}
