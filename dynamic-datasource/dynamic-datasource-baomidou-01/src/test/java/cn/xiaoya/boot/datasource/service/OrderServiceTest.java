package cn.xiaoya.boot.datasource.service;

import cn.xiaoya.boot.datasource.Baomidou01App;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 注意。使用事务必须指定数据源，否则会报错
 *
 * @author xieya
 * @date 2021/7/26
 */
@SpringBootTest(classes = Baomidou01App.class)
@RunWith(SpringRunner.class)
public class OrderServiceTest {
    @Autowired
    OrderService orderService;

    @Test
    public void method1Test() {
        orderService.method1();
    }

    @Test
    public void method2Test() {
        // 使用了事务，这儿会报错
        // Caused by: com.mysql.jdbc.exceptions.jdbc4.MySQLSyntaxErrorException: Table 'test_users.orders' doesn't exist
        /**
         * 这里，就和 Spring 事务的实现机制有关系。因为方法添加了 @Transactional 注解，Spring 事务就会生效。
         * 此时，Spring TransactionInterceptor 会通过 AOP 拦截该方法，创建事务。而创建事务，势必就会获得数据源。
         * 那么，TransactionInterceptor 会使用 Spring DataSourceTransactionManager 创建事务，
         * 并将事务信息通过 ThreadLocal 绑定在当前线程。
         * 而事务信息，就包括事务对应的 Connection 连接。
         * 那也就意味着，还没走到 OrderMapper 的查询操作，Connection 就已经被创建出来了。
         * 并且，因为事务信息会和当前线程绑定在一起，在 OrderMapper 在查询操作需要获得 Connection 时，
         * 就直接拿到当前线程绑定的 Connection ，而不是 OrderMapper 添加 @DS 注解所对应的 DataSource 所对应的 Connection 。
         * OK ，那么我们现在可以把问题聚焦到 DataSourceTransactionManager 是怎么获取 DataSource 从而获得 Connection 的了。
         * 对于每个 DataSourceTransactionManager 数据库事务管理器，创建时都会传入其需要管理的 DataSource 数据源。
         * 在使用 dynamic-datasource-spring-boot-starter 时，它创建了一个 DynamicRoutingDataSource ，
         * 传入到 DataSourceTransactionManager 中。
         * 而 DynamicRoutingDataSource 负责管理我们配置的多个数据源。
         * 例如说，本示例中就管理了 orders、users 两个数据源，并且默认使用 users 数据源。那么在当前场景下，DynamicRoutingDataSource
         * 需要基于 @DS 获得数据源名，从而获得对应的 DataSource ，
         * 结果因为我们在 Service 方法上，并没有添加 @DS 注解，所以它只好返回默认数据源，也就是 users 。
         * 故此，就发生了 Table 'test_users.orders' doesn't exist 的异常
         */
        orderService.method2();
    }

    @Test
    public void method03Test() {
        // 主类上必须使用@EnableAspectJAutoProxy(exposeProxy = true)，否则会报如下错误
        // java.lang.IllegalStateException: Cannot find current proxy:
        // Set 'exposeProxy' property on Advised to 'true' to make it available.
        orderService.method3();
    }

    @Test
    public void method4Test() {
        // method041()和method042() 中也使用了DS指定数据源，所以并不会报错
        orderService.method4();
    }

    @Test
    public void method04Test() {
        // method04 中也使用了this, 事务并没有生效，所以并不会报错
        orderService.method04();
    }


    @Test
    public void method5Test() {
        // 事务传播机制propagation = Propagation.REQUIRES_NEW，不太懂。。
        orderService.method5();
    }

    @Test
    public void method05Test() {
        // 事务传播机制，不太懂，使用默认传播机制Propagation.REQUIRED 则报错。。
        orderService.method05();
    }
}
