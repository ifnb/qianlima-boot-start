package cn.xiaoya.boot.datasource.service;

import cn.xiaoya.boot.datasource.constants.DbConstants;
import cn.xiaoya.boot.datasource.dataobject.OrderDO;
import cn.xiaoya.boot.datasource.dataobject.UserDO;
import cn.xiaoya.boot.datasource.mapper.OrderMapper;
import cn.xiaoya.boot.datasource.mapper.UserMapper;
import com.baomidou.dynamic.datasource.annotation.DS;
import org.springframework.aop.framework.AopContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author xieya
 * @date 2021/7/26
 */
@Service
public class OrderService {
    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private UserMapper userMapper;

    private OrderService self() {
        // TODO see here
        return (OrderService) AopContext.currentProxy();
    }

    public void method1() {
        // 查询订单
        OrderDO order = orderMapper.selectById(1);
        System.out.println(order);
        // 查询用户
        UserDO user = userMapper.selectById(1);
        System.out.println(user);
    }

    @Transactional(rollbackFor = Exception.class)
    public void method2() {
        // 查询用户
        UserDO user = userMapper.selectById(1);
        System.out.println(user);

        // 查询订单
        OrderDO order = orderMapper.selectById(1);
        System.out.println(order);

    }

    public void method3() {
        // 查询用户
        self().method032();
        // 查询订单 报错，因为此时获取的是 primary 对应的 DataSource ，即 users 。
        self().method031();
    }

    @Transactional(rollbackFor = Exception.class)
    public void method031() {
        OrderDO order = orderMapper.selectById(1);
        System.out.println(order);
    }

    @Transactional(rollbackFor = Exception.class)
    public void method032() {
        UserDO user = userMapper.selectById(1);
        System.out.println(user);
    }

    public void method4() {
        // 查询订单
        self().method041();
        // 查询用户
        self().method042();
    }

    public void method04() {
        // 查询订单
        this.method031();
        // 查询用户
        this.method032();
    }

    @Transactional(rollbackFor = Exception.class)
    @DS(DbConstants.DATASOURCE_ORDERS)
    public void method041() {
        OrderDO order = orderMapper.selectById(1);
        System.out.println(order);
    }

    @Transactional(rollbackFor = Exception.class)
    @DS(DbConstants.DATASOURCE_USERS)
    public void method042() {
        UserDO user = userMapper.selectById(1);
        System.out.println(user);
    }

    @Transactional(rollbackFor = Exception.class)
    @DS(DbConstants.DATASOURCE_ORDERS)
    public void method5() {
        // 查询订单
        OrderDO order = orderMapper.selectById(1);
        System.out.println(order);
        // 查询用户
        self().method051();
    }

    @Transactional(rollbackFor = Exception.class)
    @DS(DbConstants.DATASOURCE_ORDERS)
    public void method05() {
        // 查询订单
        OrderDO order = orderMapper.selectById(1);
        System.out.println(order);
        // 查询用户
        self().method052();
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    @DS(DbConstants.DATASOURCE_USERS)
    public void method051() {
        UserDO user = userMapper.selectById(1);
        System.out.println(user);
    }

    @Transactional(rollbackFor = Exception.class)
    @DS(DbConstants.DATASOURCE_USERS)
    public void method052() {
        UserDO user = userMapper.selectById(1);
        System.out.println(user);
    }
}
