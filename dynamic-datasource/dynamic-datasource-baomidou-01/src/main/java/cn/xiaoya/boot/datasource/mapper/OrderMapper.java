package cn.xiaoya.boot.datasource.mapper;

import cn.xiaoya.boot.datasource.constants.DbConstants;
import cn.xiaoya.boot.datasource.dataobject.OrderDO;
import com.baomidou.dynamic.datasource.annotation.DS;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * @author xieya
 * @date 2021/7/26
 */
@Repository
@DS(value = DbConstants.DATASOURCE_ORDERS)
public interface OrderMapper {
    /**
     * 根据ID查询订单信息
     * @param id  订单ID
     * @return
     */
    OrderDO selectById(@Param("id") Integer id);
}
