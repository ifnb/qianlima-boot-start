package cn.xiaoya.boot.datasource;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * 启动类
 * proxyTargetClass = true 将代理对象设置到AopContext中
 * @author xieya
 * @date 2021/7/26
 */
@SpringBootApplication
@EnableAspectJAutoProxy(exposeProxy = true)
@MapperScan(basePackages = "cn.xiaoya.boot.datasource.mapper")
public class Baomidou01App {
    public static void main(String[] args) {
        SpringApplication.run(Baomidou01App.class, args);
    }
}
