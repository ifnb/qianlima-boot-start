package cn.xiaoya.boot.datasource.mapper;

import cn.xiaoya.boot.datasource.constants.DbConstants;
import cn.xiaoya.boot.datasource.dataobject.UserDO;
import com.baomidou.dynamic.datasource.annotation.DS;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * @author xieya
 * @date 2021/7/26
 */
@Repository
@DS(value = DbConstants.DATASOURCE_USERS)
public interface UserMapper {
    /**
     * 根据ID查询用户信息
     * @param id 用户id
     * @return
     */
    UserDO selectById(@Param("id") Integer id);
}
