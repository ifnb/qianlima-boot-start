package cn.xiaoya.boot.jpa.dataobject;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * 用户 DO
 * @author xieya
 * @date 2021/7/26
 */
@Entity
@Table(name = "users")
@Data
public class UserDO {

    /**
     * 用户编号
     * strategy 设置使用数据库主键自增策略；
     * generator 设置插入完成后，查询最后生成的 ID 填充到该属性中。
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY,
            generator = "JDBC")
    private Long id;
    /**
     * 账号
     */
    private String username;
    private String password;

    @Column(name = "create_time")
    private Date createTime;
}
