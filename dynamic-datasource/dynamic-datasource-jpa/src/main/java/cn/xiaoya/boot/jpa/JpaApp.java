package cn.xiaoya.boot.jpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * spring data jpa 多数据源
 *
 * @author xieya
 * @date 2021/7/26
 */
@SpringBootApplication
@EnableAspectJAutoProxy(exposeProxy = true)
public class JpaApp {
    public static void main(String[] args) {
        SpringApplication.run(JpaApp.class, args);
    }
}
