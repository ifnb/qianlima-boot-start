package cn.xiaoya.boot.jpa.service;

import cn.xiaoya.boot.jpa.constants.DbConstants;
import cn.xiaoya.boot.jpa.dataobject.OrderDO;
import cn.xiaoya.boot.jpa.dataobject.UserDO;
import cn.xiaoya.boot.jpa.repository.orders.OrderRepository;
import cn.xiaoya.boot.jpa.repository.users.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.aop.framework.AopContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author xieya
 * @date 2021/7/26
 */
@Service
@Slf4j
public class OrderService {

    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private UserRepository userRepository;

    private OrderService self() {
        return (OrderService) AopContext.currentProxy();
    }

    public void method01() {
        // 查询订单
        OrderDO order = orderRepository.findById(1L).orElse(null);
        log.info("order=({})", order);
        // 查询用户
        UserDO user = userRepository.findById(1L).orElse(null);
        log.info("user=({})", user);
    }

    /**
     * 报错，找不到事务管理器
     */
    @Transactional(rollbackFor = Exception.class)
    public void method02() {
        // 查询订单
        OrderDO order = orderRepository.findById(1L).orElse(null);
        log.info("order=({})", order);
        // 查询用户
        UserDO user = userRepository.findById(1L).orElse(null);
        log.info("user=({})", user);
    }

    public void method03() {
        // 查询订单
        self().method031();
        // 查询用户
        self().method032();
    }

    @Transactional(transactionManager = DbConstants.TX_MANAGER_ORDERS, rollbackFor = Exception.class)
    public void method031() {
        OrderDO order = orderRepository.findById(1L).orElse(null);
        log.info("order=({})", order);
    }

    @Transactional(transactionManager = DbConstants.TX_MANAGER_USERS, rollbackFor = Exception.class)
    public void method032() {
        UserDO user = userRepository.findById(1L).orElse(null);
        log.info("user=({})", user);
    }

    /**
     * this 会让事务失效
     */
    public void method04() {
        // 正常访问方法2会找不到正确的事务管理器
        this.method02();
    }

    @Transactional(transactionManager = DbConstants.TX_MANAGER_ORDERS, rollbackFor = Exception.class)
    public void method05() {
        // 查询订单
        OrderDO order = orderRepository.findById(1L).orElse(null);
        log.info("order=({})", order);
        // 查询用户
        self().method052();
    }

    @Transactional(transactionManager = DbConstants.TX_MANAGER_USERS,
            propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public void method052() {
        UserDO user = userRepository.findById(1L).orElse(null);
        log.info("user=({})", user);
    }

}
