package cn.xiaoya.boot.jpa.repository.orders;

import cn.xiaoya.boot.jpa.dataobject.OrderDO;
import org.springframework.data.repository.CrudRepository;

/**
 * order repository
 *
 * @author xieya
 * @date 2021/7/26
 */
public interface OrderRepository extends CrudRepository<OrderDO, Long> {

}
