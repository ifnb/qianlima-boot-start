package cn.xiaoya.boot.jpa.repository.users;

import cn.xiaoya.boot.jpa.dataobject.UserDO;
import org.springframework.data.repository.CrudRepository;

/**
 * user repository
 * @author xieya
 * @date 2021/7/26
 */
public interface UserRepository extends CrudRepository<UserDO, Long> {

}
