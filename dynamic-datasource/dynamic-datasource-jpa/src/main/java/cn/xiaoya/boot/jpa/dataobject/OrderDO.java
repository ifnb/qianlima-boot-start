package cn.xiaoya.boot.jpa.dataobject;


import lombok.Data;

import javax.persistence.*;

/**
 * 订单 DO
 * @author xieya
 * @date 2021/7/26
 */
@Entity
@Table(name = "orders")
@Data
public class OrderDO {

    /**
     * 订单编号
     * strategy 设置使用数据库主键自增策略；
     * generator 设置插入完成后，查询最后生成的 ID 填充到该属性中。
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY,
            generator = "JDBC")
    private Long id;
    /**
     * 用户编号
     */
    @Column(name = "user_id")
    private Long userId;

}
