package cn.xiaoya.boot.jpa.service;

import cn.xiaoya.boot.jpa.JpaApp;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author xieya
 * @date 2021/7/26
 */
@SpringBootTest(classes = JpaApp.class)
@RunWith(SpringRunner.class)
public class OrderServiceTest {
    @Autowired
    OrderService orderService;

    @Test
    public void test1() {
        orderService.method01();
    }

    @Test
    public void test2() {
        // 报错，找不到事务管理器
        /**
         * org.springframework.beans.factory.NoUniqueBeanDefinitionException:
         * No qualifying bean of type 'org.springframework.transaction.PlatformTransactionManager' available:
         * expected single matching bean but found 2: ordersTransactionManager,usersTransactionManager
         */
        orderService.method02();
    }

    @Test
    public void test3() {
        // 指定了对应的事务管理器，正常运行
        orderService.method03();
    }

    @Test
    public void test4() {
        // 事务失效 正常运行
        orderService.method04();
    }

    @Test
    public void test5() {
        // 事务隔离？不懂
        orderService.method05();
    }

}
