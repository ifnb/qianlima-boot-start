package cn.xiaoya.boot.sharding1.service;

import cn.xiaoya.boot.sharding1.ShardingJdbcApp;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * TODO 多数据源下使用 sharding-jdbc 是最好的事务解决方案 没有之一
 *
 * @author xieya
 * @date 2021/7/26
 */
@SpringBootTest(classes = ShardingJdbcApp.class)
@RunWith(SpringRunner.class)
public class OrderServiceTest {
    @Autowired
    OrderService orderService;

    @Test
    public void method1Test() {
        orderService.method1();
    }

    @Test
    public void method2Test() {
        // TODO　使用了事务，这儿不会报错
        orderService.method2();
    }

    @Test
    public void method03Test() {
        // 主类上必须使用@EnableAspectJAutoProxy(exposeProxy = true)，否则会报如下错误
        // java.lang.IllegalStateException: Cannot find current proxy:
        // Set 'exposeProxy' property on Advised to 'true' to make it available.
        orderService.method3();
    }

    @Test
    public void method4Test() {
        //
        orderService.method4();
    }

    @Test
    public void method04Test() {
        // method04 中也使用了this, 事务并没有生效，所以并不会报错
        orderService.method04();
    }


    @Test
    public void method5Test() {
        // 事务传播机制propagation
        orderService.method5();
    }

    @Test
    public void method05Test() {
        // 事务传播机制，不太懂
        orderService.method05();
    }
}
