package cn.xiaoya.boot.sharding1;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * ShardingJdbc多数据源
 *
 * @author xieya
 * @date 2021/7/27
 */
@SpringBootApplication
@MapperScan(basePackages = "cn.xiaoya.boot.sharding1.mapper")
@EnableAspectJAutoProxy(exposeProxy = true)
public class ShardingJdbcApp {
    public static void main(String[] args) {
        SpringApplication.run(ShardingJdbcApp.class);
    }
}
