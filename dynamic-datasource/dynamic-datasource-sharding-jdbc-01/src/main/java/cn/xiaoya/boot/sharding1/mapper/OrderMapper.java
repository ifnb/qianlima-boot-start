package cn.xiaoya.boot.sharding1.mapper;


import cn.xiaoya.boot.sharding1.dataobject.OrderDO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * @author xieya
 * @date 2021/7/26
 */
@Repository
public interface OrderMapper {
    /**
     * 根据ID查询订单信息
     * @param id  订单ID
     * @return
     */
    OrderDO selectById(@Param("id") Integer id);
}
