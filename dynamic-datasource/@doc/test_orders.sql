/*
 Navicat Premium Data Transfer

 Source Server         : 本地连接
 Source Server Type    : MySQL
 Source Server Version : 50727
 Source Host           : localhost:3306
 Source Schema         : test_orders

 Target Server Type    : MySQL
 Target Server Version : 50727
 File Encoding         : 65001

 Date: 28/07/2021 09:44:14
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders`  (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '订单编号',
  `user_id` bigint(16) NULL DEFAULT NULL COMMENT '用户编号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '订单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES (1, 1);
INSERT INTO `orders` VALUES (2, 10);
INSERT INTO `orders` VALUES (3, 10);
INSERT INTO `orders` VALUES (4, 20);
INSERT INTO `orders` VALUES (5, 20);
INSERT INTO `orders` VALUES (6, 20);
INSERT INTO `orders` VALUES (7, 266);
INSERT INTO `orders` VALUES (8, 266);
INSERT INTO `orders` VALUES (9, 99);

SET FOREIGN_KEY_CHECKS = 1;
