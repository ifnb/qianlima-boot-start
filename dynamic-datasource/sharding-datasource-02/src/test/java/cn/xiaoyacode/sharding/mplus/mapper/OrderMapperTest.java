package cn.xiaoyacode.sharding.mplus.mapper;

import cn.xiaoyacode.sharding.mplus.MybatisPlusApp;
import cn.xiaoyacode.sharding.mplus.dataobject.OrderDO;
import org.apache.shardingsphere.api.hint.HintManager;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author xieya
 * @date 2021/7/28
 */
@SpringBootTest(classes = MybatisPlusApp.class)
@RunWith(SpringRunner.class)
public class OrderMapperTest {
    @Autowired
    private OrderMapper orderMapper;

    @Test
    public void testSelectById() {
        // 测试从库的负载均衡
        for (int i = 0; i < 5; i++) {
            OrderDO order = orderMapper.selectById(1);
            System.out.println(order);
        }
    }

    @Test
    public void testSelectById02() {
        // 测试强制访问主库
        try (HintManager hintManager = HintManager.getInstance()) {
            // 设置强制访问主库
            hintManager.setMasterRouteOnly();
            // 执行查询
            OrderDO order = orderMapper.selectById(1);
            System.out.println(order);
        }
    }

    @Test
    public void testInsert() {
        // 插入
        OrderDO order = new OrderDO();
        order.setUserId(110);
        orderMapper.insert(order);
    }
}
