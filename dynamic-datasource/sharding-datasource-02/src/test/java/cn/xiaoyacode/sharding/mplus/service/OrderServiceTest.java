package cn.xiaoyacode.sharding.mplus.service;

import cn.xiaoyacode.sharding.mplus.MybatisPlusApp;
import cn.xiaoyacode.sharding.mplus.dataobject.OrderDO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author xieya
 * @date 2021/7/28
 */
@SpringBootTest(classes = MybatisPlusApp.class)
@RunWith(SpringRunner.class)
public class OrderServiceTest {
    @Autowired
    private OrderService orderService;

    @Test
    public void testAdd() {
        OrderDO order = new OrderDO();
        order.setUserId(20);
        orderService.add(order);
    }

    @Test
    public void testFindById() {
        // 默认是查从库，并且是轮询查询
        OrderDO order = orderService.findById(1);
        OrderDO order2 = orderService.findById(1);
        System.out.println(order);
        System.out.println(order2);
    }
}
