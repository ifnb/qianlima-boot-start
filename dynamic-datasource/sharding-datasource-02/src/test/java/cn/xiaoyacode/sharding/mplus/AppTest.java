package cn.xiaoyacode.sharding.mplus;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author xieya
 * @date 2021/7/28
 */
@SpringBootTest(classes = MybatisPlusApp.class)
@RunWith(SpringRunner.class)
public class AppTest {
}
