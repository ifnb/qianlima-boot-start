package cn.xiaoyacode.sharding.mplus;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author xieya
 * @date 2021/7/28
 */
@SpringBootApplication
@MapperScan(basePackages = "cn.xiaoyacode.sharding.mplus.mapper")
public class MybatisPlusApp {
    public static void main(String[] args) {
        SpringApplication.run(MybatisPlusApp.class, args);
    }
}
