package cn.xiaoyacode.sharding.mplus.mapper;

import cn.xiaoyacode.sharding.mplus.dataobject.OrderDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * @author xieya
 * @date 2021/7/28
 */
@Repository
public interface OrderMapper extends BaseMapper<OrderDO> {

}
