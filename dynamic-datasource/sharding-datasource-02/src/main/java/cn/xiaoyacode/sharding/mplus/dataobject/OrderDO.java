package cn.xiaoyacode.sharding.mplus.dataobject;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 必须指定 TableName
 * @author xieya
 * @date 2021/7/28
 */
@Data
@TableName(value = "orders")
public class OrderDO {
    /**
     * 订单编号
     */
    private Long id;
    /**
     * 用户编号
     */
    private Integer userId;
}
