## Read Me First
* 项目名称：ddd-boot-demo
* 项目描述：基于DDD架构的Demo，基于SpringBoot落地
## 项目架构
- ui —— User Interface.门面层，对外以各种协议提供服务，该层需要明确定义支持的服务协议、契约等
- application —— 应用服务层，组合domain层的领域对象和基础设施层的公共组件，根据业务需要包装出多变的服务，以适应多变的业务服务需求。
- domain —— 业务领域层，是我们最应当关心的一层，也是最多变的一层，需要保证这一层是高内聚的。确保所有的业务逻辑都留在这一层，而不会遗漏到其他层。按照ddd（domain driven design）理论，主要有如下概念构成：entity、value object、domain service、factory、domain event、repository
- infrastructure —— 基础设施层提供公共功能组件，供controller、service、domain层调用。比如：持久化相关组件、httpclent、validation、checkLogint、exceptionHanler、message resource等。

- converter —— 通用转换器，用于dto、entity、po之前的转换，不属于DDD架构的一层，但是至关重要!

# Getting Started

### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.5.2/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.5.2/maven-plugin/reference/html/#build-image)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.5.2/reference/htmlsingle/#boot-features-developing-web-applications)

### Guides
The following guides illustrate how to use some features concretely:

* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/bookmarks/)

