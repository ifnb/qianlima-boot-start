package cn.xiaoyacode.ddd.hutool;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.Month;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.lang.Console;
import org.junit.jupiter.api.Test;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.util.Calendar;
import java.util.Date;

/**
 * @author xieya
 * @date 2021/7/21
 */
public class HuToolTest {
    @Test
    public void test() {
        double a = 123456789.66;
        //结果为："壹亿贰仟叁佰肆拾伍万陆仟柒佰捌拾玖元陆角陆分"
        String digitUppercase = Convert.digitToChinese(a);
        System.out.println(digitUppercase);
    }

    @Test
    public void TimeTest() {
        //当前时间
        Date date = DateUtil.date();
        //当前时间
        Date date2 = DateUtil.date(Calendar.getInstance());
        //当前时间
        Date date3 = DateUtil.date(System.currentTimeMillis());
        //当前时间字符串，格式：yyyy-MM-dd HH:mm:ss
        String now = DateUtil.now();
        //当前日期字符串，格式：yyyy-MM-dd
        String today = DateUtil.today();
        Date today1 = DateUtil.parse(now, "yyyy-MM-dd HH:mm:ss");
        System.out.println(DateUtil.dayOfMonth(today1));
        System.out.println(DateUtil.beginOfDay(today1));
        System.out.println(DateUtil.endOfDay(today1));
        int year = DateUtil.year(today1);
        System.out.println(DateUtil.isLeapYear(year));

    }

    @Test
    public void dateTimeTest() {
        Date date = new Date();
        //new方式创建
        DateTime time = new DateTime(date);
        Console.log(time);
        //of方式创建
        DateTime now = DateTime.now();
        DateTime dt = DateTime.of(date);
        Console.log(dt.year());
        Console.log(dt.monthEnum());
        int lastDay = Month.getLastDay(dt.month(), dt.isLeapYear());
        Console.log(lastDay);
    }

    @Test
    public void copyFileTest() {
        // 复制文件 in => out
        BufferedInputStream in = FileUtil.getInputStream("d:/test.txt");
        BufferedOutputStream out = FileUtil.getOutputStream("d:/test2.txt");
        long copySize = IoUtil.copy(in, out, IoUtil.DEFAULT_BUFFER_SIZE);
        Console.log(copySize);
    }

    @Test
    public void copyFileTest2() {
        // 复制文件 in => out
        BufferedInputStream in = FileUtil.getInputStream("d:/1.pdf");
        BufferedOutputStream out = FileUtil.getOutputStream("d:/2.pdf");
        long copySize = IoUtil.copy(in, out, IoUtil.DEFAULT_BUFFER_SIZE);
        Console.log(copySize);
    }
}
