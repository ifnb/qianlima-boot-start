package cn.xiaoyacode.ddd.application;

import cn.xiaoyacode.ddd.application.user.service.UserApplicationService;
import cn.xiaoyacode.ddd.infrastructure.common.vo.CommonResult;
import cn.xiaoyacode.ddd.ui.dto.user.req.UserCreateReqDTO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author xieya
 * @date 2021/7/21
 */
@SpringBootTest
@Slf4j
public class UserApplicationServiceTest {
    @Autowired
    UserApplicationService userApplicationService;

    @Test
    public void createUserTest() {
        UserCreateReqDTO reqParam = new UserCreateReqDTO()
                .setUsername("libai")
                .setEmail("11254@qq.com")
                .setAge(18888);
        CommonResult<Long> result = userApplicationService.createUser(reqParam);
        log.info("result= {}", result);
    }
}

