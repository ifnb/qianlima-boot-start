package cn.xiaoyacode.ddd.async;

import cn.hutool.core.lang.Console;
import cn.xiaoyacode.ddd.domain.demo.service.DemoService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * @author xieya
 * @date 2021/7/22
 */
@SpringBootTest
public class AsyncTest {

    @Resource
    DemoService demoService;

    @Test
    public void test() {
        Console.log("正常调用了1");
        Console.log("正常调用了2");
        Console.log("正常调用了3");
        Console.log("正常调用了4");
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        executorService.execute(() -> {
            Console.log("我被异步调用了");
        });
        executorService.execute(() -> {
            Console.log("我被异步调用了A");
        });
        executorService.execute(() -> {
            Console.log("我被异步调用了B");
        });
        executorService.execute(() -> {
            Console.log("我被异步调用了C");
        });
        executorService.shutdown();
    }

    @Test
    public void task01() {
        long now = System.currentTimeMillis();
        Console.log("execute01开始执行");
        // sleep 10s
        demoService.execute01();
        Console.log("execute01执行结束,耗时{}ms", System.currentTimeMillis() - now);

        Console.log("execute02开始执行");
        // sleep 5s
        demoService.execute02();
        Console.log("execute02执行结束,耗时{}ms", System.currentTimeMillis() - now);
    }

    @Test
    public void task02() {
        long now = System.currentTimeMillis();
        Console.log("异步的executeAsync01开始执行");
        // sleep 10s
        demoService.executeAsync01();
        Console.log("异步的executeAsync01执行结束,耗时{}ms", System.currentTimeMillis() - now);

        Console.log("异步的executeAsync02开始执行");
        // sleep 5s
        demoService.executeAsync02();
        Console.log("异步的executeAsync02执行结束,耗时{}ms", System.currentTimeMillis() - now);
    }

    @Test
    public void test03() throws ExecutionException, InterruptedException {
        long now = System.currentTimeMillis();
        Console.log("execute开始执行");
        Future<Integer> execute01Result = demoService.execute01AsyncWithFuture();
        Future<Integer> execute02Result = demoService.execute02AsyncWithFuture();
        // 这儿需要阻塞，等待返回的结果
        execute01Result.get();
        execute02Result.get();
        Console.log("execute执行结束,耗时{}ms", System.currentTimeMillis() - now);
    }
}
