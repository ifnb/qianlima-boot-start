package cn.xiaoyacode.ddd.repository;

import cn.xiaoyacode.ddd.domain.user.model.UserEntity;
import cn.xiaoyacode.ddd.domain.user.model.vo.Email;
import cn.xiaoyacode.ddd.domain.user.model.vo.UserId;
import cn.xiaoyacode.ddd.domain.user.model.vo.UserName;
import cn.xiaoyacode.ddd.infrastructure.persistence.mysql.UserRepositoryImpl;
import cn.xiaoyacode.ddd.infrastructure.persistence.mysql.mapper.user.UserMapper;
import cn.xiaoyacode.ddd.infrastructure.persistence.mysql.po.user.UserPO;
import cn.xiaoyacode.ddd.ui.dto.user.req.UserListQueryReqDTO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @author xieya
 * @date 2021/7/16
 */
@SpringBootTest
@Slf4j
public class UserRepositoryImplTest {
    @Autowired
    UserRepositoryImpl userRepository;
    @Autowired
    UserMapper userMapper;

    @Test
    public void createUserTest() {
        UserId userId = userRepository.createUser(new UserEntity()
                .setEmail(new Email("119@qq.com"))
                .setUsername(new UserName("test66")).setAge(12));
        log.info("userId = {}", userId.getUserId());
    }

    @Test
    public void listAllUsersTest() {
        List<UserPO> userBOList = userMapper.selectUserList(new UserListQueryReqDTO().setUsername("test"));
        log.info("UserList = {}", userBOList);
    }
}
