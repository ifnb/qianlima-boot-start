package cn.xiaoyacode.ddd.application.user.service.impl;

import cn.xiaoyacode.ddd.application.user.service.UserApplicationService;
import cn.xiaoyacode.ddd.domain.user.factory.UserFactory;
import cn.xiaoyacode.ddd.domain.user.model.UserEntity;
import cn.xiaoyacode.ddd.domain.user.model.enums.DeleteFlagEnum;
import cn.xiaoyacode.ddd.domain.user.model.enums.UserStatusEnum;
import cn.xiaoyacode.ddd.domain.user.model.vo.Email;
import cn.xiaoyacode.ddd.domain.user.model.vo.UserName;
import cn.xiaoyacode.ddd.domain.user.repository.IUserRepository;
import cn.xiaoyacode.ddd.infrastructure.common.vo.CommonResult;
import cn.xiaoyacode.ddd.ui.dto.user.req.UserCreateReqDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author xieya
 * @date 2021/7/16
 */
@Service
public class UserApplicationServiceImpl implements UserApplicationService {

    @Autowired
    private IUserRepository userRepository;

    @Override
    public CommonResult<Long> createUser(UserCreateReqDTO userCreateReqDTO) {
        UserFactory userFactory = new UserFactory(userRepository);
        UserEntity user = userFactory.createUser(new Email(userCreateReqDTO.getEmail()),
                new UserName(userCreateReqDTO.getUsername()), userCreateReqDTO.getAge())
                .setStatus(UserStatusEnum.ENABLE).setDeleted(DeleteFlagEnum.NO);
        return CommonResult.success(userRepository.createUser(user).getUserId());
    }
}
