package cn.xiaoyacode.ddd.infrastructure.constants.user;

import cn.xiaoyacode.ddd.infrastructure.common.exception.ErrorCode;

/**
 * user错误码常量配置
 *
 * @author xieya
 * @date 2021/7/15
 */
public interface UserErrorCodeConstants {
    ErrorCode USER_IS_EXISTS_ERROR = new ErrorCode(1000002000, "该用户名已经被使用");
}
