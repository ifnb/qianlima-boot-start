package cn.xiaoyacode.ddd.infrastructure.common.core.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.core.injector.DefaultSqlInjector;
import com.baomidou.mybatisplus.core.injector.ISqlInjector;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * mybatis plus 配置
 * MapperScan 扫描包所有Mapper接口
 * EnableTransactionManagement 开启事务管理器
 *
 * @author xieya
 * @date 2021/7/15
 */
@MapperScan(basePackages = "cn.xiaoyacode.ddd.demo.infrastructure.repository.mysql.mapper")
@EnableTransactionManagement(proxyTargetClass = true)
public class DatabaseConfiguration {
    // 数据库连接池 Druid

    @Bean
    public ISqlInjector sqlInjector() {
        // MyBatis Plus 逻辑删除
        return new DefaultSqlInjector();
    }

    /**
     * 最新版使用
     *
     * @return
     */
    @Bean
    public MybatisPlusInterceptor paginationInterceptor() {
        // MyBatis Plus 分页插件
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
        return interceptor;
    }
}
