package cn.xiaoyacode.ddd.domain.user.model.enums;

import lombok.Getter;

/**
 * @author xieya
 * @date 2021/7/21
 */
@Getter
public enum UserStatusEnum {

    /**
     * 启用
     */
    ENABLE(1),
    /**
     * 禁用
     */
    DISABLE(0);

    private final Integer code;

    UserStatusEnum(Integer code) {
        this.code = code;
    }

}
