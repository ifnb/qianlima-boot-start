package cn.xiaoyacode.ddd.domain.post.model.vo;

import cn.xiaoyacode.ddd.infrastructure.common.domain.ValueObject;
import cn.xiaoyacode.ddd.infrastructure.common.exception.util.ServiceExceptionUtil;
import cn.xiaoyacode.ddd.infrastructure.constants.post.PostErrorCodeConstants;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

/**
 * 帖子标题
 *
 * @author xieya
 * @date 2021/7/19
 */
@Data
public class PostTitle implements ValueObject<PostTitle> {
    private final String title;

    public PostTitle(String title) {
        // 文章标题不能为空
        ServiceExceptionUtil.must(StringUtils.isBlank(title), PostErrorCodeConstants.POST_TITLE_IS_NULL);
        this.title = title;
    }

    @Override
    public boolean sameValueAs(PostTitle other) {
        return other != null && this.title.equals(other.title);
    }
}
