package cn.xiaoyacode.ddd.domain.post.model;

import cn.xiaoyacode.ddd.infrastructure.common.exception.util.ServiceExceptionUtil;
import cn.xiaoyacode.ddd.infrastructure.constants.post.PostErrorCodeConstants;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

/**
 * 帖子状态枚举类
 *
 * @author xieya
 */
@Getter
@ToString
@Slf4j
public enum PostStatus {
    /**
     * 已发布
     */
    HAS_POSTED("00", "已发布"),

    /**
     * 等待运营审核
     */
    WAIT_VERIFY("01", "等待运营审核"),

    /**
     * 已删除
     */
    HAS_DELETED("99", "已删除");

    private final String code;

    private final String description;

    PostStatus(String code, String description) {
        this.code = code;
        this.description = description;
    }

    /**
     * 根据帖子code获取帖子详细
     *
     * @param code 帖子状态code标识
     * @return PostStatus
     * @throws IllegalArgumentException
     */
    public static PostStatus getPostStatus(String code) throws IllegalArgumentException {
        for (PostStatus postStatus : values()) {
            if (postStatus.code.equals(code)) {
                return postStatus;
            }
        }
        log.warn("Can not find a PostStatus for this code: {}" + code);
        throw ServiceExceptionUtil.exception(PostErrorCodeConstants.POST_STATUS_CODE_NOT_FOUND);
    }
}
