package cn.xiaoyacode.ddd.infrastructure.converter.post;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * @author xieya
 * @date 2021/7/14
 */
@Mapper(componentModel = "spring")
public interface PostConvert {

    PostConvert INSTANCE = Mappers.getMapper(PostConvert.class);
}
