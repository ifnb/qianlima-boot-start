package cn.xiaoyacode.ddd.infrastructure.persistence.mysql.mapper.post;

import cn.xiaoyacode.ddd.infrastructure.persistence.mysql.po.post.PostPO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author xieya
 * @date 2021/7/14
 */
public interface PostMapper extends BaseMapper<PostPO> {
}
