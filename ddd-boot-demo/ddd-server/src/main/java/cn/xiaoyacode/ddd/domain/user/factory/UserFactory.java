package cn.xiaoyacode.ddd.domain.user.factory;

import cn.xiaoyacode.ddd.domain.user.model.UserEntity;
import cn.xiaoyacode.ddd.domain.user.model.vo.Email;
import cn.xiaoyacode.ddd.domain.user.model.vo.UserName;
import cn.xiaoyacode.ddd.domain.user.repository.IUserRepository;
import cn.xiaoyacode.ddd.infrastructure.common.exception.util.ServiceExceptionUtil;
import cn.xiaoyacode.ddd.infrastructure.constants.user.UserErrorCodeConstants;

/**
 * @author xieya
 * @date 2021/7/21
 */
public class UserFactory {
    private IUserRepository userRepository;

    public UserFactory(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public UserEntity createUser(Email email, UserName userName, Integer age) {
        // 检查用户名是否存在
        ServiceExceptionUtil.must(
                userRepository.isExistUser(userName.getUsername()), UserErrorCodeConstants.USER_IS_EXISTS_ERROR);
        return new UserEntity()
                .setUsername(userName)
                .setEmail(email)
                .setAge(age);
    }
}
