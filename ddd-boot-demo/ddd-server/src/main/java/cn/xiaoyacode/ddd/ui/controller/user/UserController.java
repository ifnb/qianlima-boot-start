package cn.xiaoyacode.ddd.ui.controller.user;

import cn.xiaoyacode.ddd.application.user.service.UserApplicationService;
import cn.xiaoyacode.ddd.infrastructure.common.vo.CommonResult;
import cn.xiaoyacode.ddd.ui.dto.user.req.UserCreateReqDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xieya
 * @date 2021/7/13
 */
@RestController
@RequestMapping("user")
public class UserController {
    @Autowired
    UserApplicationService userApplicationService;

    /**
     * 创建用户
     * POST http://127.0.0.1:17100/api/user/creat
     *
     * @param userCreateReqDTO 创建用户请求参数
     * @return
     */
    @PostMapping("/creat")
    public CommonResult<Long> creat(@Validated UserCreateReqDTO userCreateReqDTO) {
        return userApplicationService.createUser(userCreateReqDTO);
    }
}
