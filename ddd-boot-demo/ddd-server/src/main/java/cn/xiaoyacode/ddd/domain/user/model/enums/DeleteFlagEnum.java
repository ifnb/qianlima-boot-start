package cn.xiaoyacode.ddd.domain.user.model.enums;

import lombok.Getter;

/**
 * @author xieya
 * @date 2021/7/21
 */
@Getter
public enum DeleteFlagEnum {
    /**
     * 删除
     */
    YES(1),
    /**
     * 正常
     */
    NO(0);

    private final Integer code;

    DeleteFlagEnum(Integer code) {
        this.code = code;
    }
}
