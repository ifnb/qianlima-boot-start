package cn.xiaoyacode.ddd.ui.dto.user.req;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author xieya
 * @date 2021/7/20
 */
@Data
@Accessors(chain = true)
public class UserListQueryReqDTO {
    private Long userId;
    private String username;
}
