package cn.xiaoyacode.ddd.domain.demo.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;

import java.util.concurrent.Future;

/**
 * @author xieya
 * @date 2021/7/22
 */
@Slf4j
@Component
public class DemoService {


    private static void sleep(int seconds) {
        try {
            Thread.sleep(seconds * 1000L);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public Integer execute01() {
        log.info("[execute01]");
        sleep(10);
        return 1;
    }

    public Integer execute02() {
        log.info("[execute02]");
        sleep(5);
        return 2;
    }

    @Async
    public Integer executeAsync01() {
        return this.execute01();
    }

    @Async
    public Integer executeAsync02() {
        return this.execute02();
    }


    @Async
    public Future<Integer> execute01AsyncWithFuture() {
        return AsyncResult.forValue(this.execute01());
    }

    @Async
    public Future<Integer> execute02AsyncWithFuture() {
        return AsyncResult.forValue(this.execute02());
    }
}


