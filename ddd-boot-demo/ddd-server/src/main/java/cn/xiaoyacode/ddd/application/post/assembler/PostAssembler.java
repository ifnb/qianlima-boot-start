package cn.xiaoyacode.ddd.application.post.assembler;


import cn.xiaoyacode.ddd.domain.post.model.PostEntity;
import cn.xiaoyacode.ddd.ui.dto.post.req.PostCreateReqDTO;
import cn.xiaoyacode.ddd.ui.dto.post.res.PostRespDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * @author xieya
 * @date 2021/7/20
 */
@Mapper
public interface PostAssembler {
    PostAssembler INSTANCE = Mappers.getMapper(PostAssembler.class);

    default PostEntity convertPostCreate(PostCreateReqDTO postCreateReqDTO) {
        return new PostEntity();
    }

    default PostRespDTO convertResp(PostEntity post) {
        return new PostRespDTO();
    }
}
