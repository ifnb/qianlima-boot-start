package cn.xiaoyacode.ddd.infrastructure.constants.post;

import cn.xiaoyacode.ddd.infrastructure.common.exception.ErrorCode;

/**
 * post错误码常量配置
 *
 * @author xieya
 * @date 2021/7/15
 */
public interface PostErrorCodeConstants {
    ErrorCode POST_CONTENT_OVER_LIMIT = new ErrorCode(1000001000, "帖子内容不少于16个字");
    ErrorCode POST_STATUS_CODE_NOT_FOUND = new ErrorCode(1000001001, "帖子状态码不存在");
    ErrorCode POST_TITLE_IS_NULL = new ErrorCode(1000001002, "帖子标题不能为空");
    ErrorCode POST_AUTHOR_ID_IS_NULL = new ErrorCode(1000001003, "帖子作者ID不能为空");
}
