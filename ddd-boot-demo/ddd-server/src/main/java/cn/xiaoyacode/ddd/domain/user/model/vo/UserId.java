package cn.xiaoyacode.ddd.domain.user.model.vo;

import cn.xiaoyacode.ddd.infrastructure.common.domain.ValueObject;
import lombok.Getter;
import lombok.ToString;

/**
 * @author xieya
 * @date 2021/7/21
 */
@Getter
@ToString
public class UserId implements ValueObject<UserId> {
    private final Long userId;

    public UserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public boolean sameValueAs(UserId other) {
        return other != null && other.userId.equals(this.userId);
    }
}
