package cn.xiaoyacode.ddd.ui.dto.user.req;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author xieya
 * @date 2021/7/16
 */
@Data
@Accessors(chain = true)
public class UserCreateReqDTO implements Serializable {

    private static final long serialVersionUID = -7946671912031145496L;
    @NotBlank(message = "用户名不能为空")
    private String username;

    @NotNull(message = "年龄不能为空")
    @Min(value = 1, message = "年龄最小为1")
    @Max(value = 100, message = "年龄最大为100")
    private Integer age;

    @NotBlank(message = "邮箱不能为空")
    private String email;
}
