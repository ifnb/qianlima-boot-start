package cn.xiaoyacode.ddd.domain.user.model;

import cn.xiaoyacode.ddd.domain.user.model.enums.DeleteFlagEnum;
import cn.xiaoyacode.ddd.domain.user.model.enums.UserStatusEnum;
import cn.xiaoyacode.ddd.domain.user.model.vo.Email;
import cn.xiaoyacode.ddd.domain.user.model.vo.UserName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author xieya
 * @date 2021/7/13
 */
@Data
@Accessors(chain = true)
public class UserEntity {
    private Long userId;

    private UserName username;

    private Integer age;

    private Email email;

    private UserStatusEnum status;

    private DeleteFlagEnum deleted;
}
