package cn.xiaoyacode.ddd.ui.controller.post;

import cn.xiaoyacode.ddd.application.post.service.PostServiceApp;
import cn.xiaoyacode.ddd.infrastructure.common.vo.CommonResult;
import cn.xiaoyacode.ddd.ui.dto.post.req.PostCreateReqDTO;
import cn.xiaoyacode.ddd.ui.dto.post.res.PostRespDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 帖子API
 *
 * @author xieya
 * @lilk 设计参见 https://github.com/daoqidelv/community-ddd-demo
 * @link 设计参见 https://github.com/YunaiV/onemall
 * @link 设计参见 https://github.com/jovezhao/nest-plus
 * @link 设计参见 https://gitee.com/xtoon/xtoon-cloud
 * @link 设计参见 https://gitee.com/xtoon/xtoon-boot
 * @date 2021/7/13
 */
@RestController
@RequestMapping("post")
public class PostController {
    @Autowired
    PostServiceApp postService;

    /**
     * 创建帖子
     * POST http://127.0.0.1:17100/api/post/posting
     *
     * @param postCreateReqDTO 帖子创建请求参数
     * @return 创建成功返回帖子信息，失败则提示用户异常信息
     */
    @PostMapping("/posting")
    public CommonResult<PostRespDTO> posting(@Validated PostCreateReqDTO postCreateReqDTO) {
        return CommonResult.success(new PostRespDTO()
                .setSourceContent(postCreateReqDTO.getSourceContent())
                .setTitle(postCreateReqDTO.getTitle()));
    }
}
