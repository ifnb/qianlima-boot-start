/**
 * 组装器，用于组装业务业务返回结果
 *
 * @author xieya
 * @date 2021/7/13
 */
package cn.xiaoyacode.ddd.application.post.assembler;
