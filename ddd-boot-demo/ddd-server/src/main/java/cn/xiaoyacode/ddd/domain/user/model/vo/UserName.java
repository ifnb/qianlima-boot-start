package cn.xiaoyacode.ddd.domain.user.model.vo;

import cn.xiaoyacode.ddd.infrastructure.common.domain.ValueObject;
import lombok.Getter;
import lombok.ToString;

/**
 * @author xieya
 * @date 2021/7/21
 */
@Getter
@ToString
public class UserName implements ValueObject<UserName> {
    private final String username;

    public UserName(String username) {
        this.username = username;
    }

    @Override
    public boolean sameValueAs(UserName other) {
        return other != null && other.username.equals(this.username);
    }
}
