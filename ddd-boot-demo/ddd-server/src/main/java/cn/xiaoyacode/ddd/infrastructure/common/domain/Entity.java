package cn.xiaoyacode.ddd.infrastructure.common.domain;

import java.io.Serializable;

/**
 * 领域实体
 *
 * @author xieya
 * @date 2021/7/19
 */
public interface Entity<T> extends Serializable {
    /**
     * 实体 通过身份比较 而不是属性进行比较。
     *
     * @param other 另一个实体
     * @return 如果身份相同，则无论其他属性如何，都为 true。
     */
    boolean sameIdentityAs(T other);
}
