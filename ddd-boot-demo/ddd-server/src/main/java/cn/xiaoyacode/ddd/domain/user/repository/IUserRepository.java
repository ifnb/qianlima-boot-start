package cn.xiaoyacode.ddd.domain.user.repository;

import cn.xiaoyacode.ddd.domain.user.model.UserEntity;
import cn.xiaoyacode.ddd.domain.user.model.vo.UserId;

/**
 * @author xieya
 * @date 2021/7/16
 */
public interface IUserRepository {
    /**
     * 创建用户
     *
     * @param user 创建用户参数
     * @return
     */
    UserId createUser(UserEntity user);

    /**
     * 存在true,不存在false
     *
     * @param username 用户名
     * @return
     */
    boolean isExistUser(String username);
}
