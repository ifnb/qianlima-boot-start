package cn.xiaoyacode.ddd.infrastructure.constants.post;

/**
 * @author xieya
 * @date 2021/7/19
 */
public class PostConstants {
    public static final Integer POST_SOURCE_CONTENT_MIN_LENGTH = 16;
}
