package cn.xiaoyacode.ddd.domain.user.model.vo;

import cn.xiaoyacode.ddd.infrastructure.common.domain.ValueObject;
import lombok.Getter;
import lombok.ToString;

/**
 * @author xieya
 * @date 2021/7/21
 */
@Getter
@ToString
public class Email implements ValueObject<Email> {
    private final String email;

    public Email(String email) {
        this.email = email;
    }

    @Override
    public boolean sameValueAs(Email other) {
        return other != null && other.email.equals(this.email);
    }
}
