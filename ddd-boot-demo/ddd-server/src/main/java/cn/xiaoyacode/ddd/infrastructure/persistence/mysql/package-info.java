/**
 * dao层次，对数据库的访问
 * 实现可以有 jpa | mybatis-> tk.mybatis -> mybatis plus
 * 普通项目 tk.mybatis+自定义xml
 * 特别： mybatis plus 也是业界主流
 *
 * @author xieya
 * @date 2021/7/14
 */
package cn.xiaoyacode.ddd.infrastructure.persistence.mysql;
