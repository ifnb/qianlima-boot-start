package cn.xiaoyacode.ddd.ui.dto.post.res;

import cn.xiaoyacode.ddd.domain.post.model.PostStatus;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author xieya
 * @date 2021/7/13
 */
@Data
@Accessors(chain = true)
public class PostRespDTO implements Serializable {
    private static final long serialVersionUID = 4557867275845417149L;

    /**
     * 帖子id
     */
    @JSONField(ordinal = 10)
    private Long postId;

    /**
     * 帖子作者
     */
    @JSONField(ordinal = 20)
    private Long authorId;

    /**
     * 帖子标题
     */
    @JSONField(ordinal = 30)
    private String title;

    /**
     * 帖子源内容
     */
    @JSONField(ordinal = 40)
    private String sourceContent;

    /**
     * 发帖时间
     */
    @JSONField(ordinal = 50)
    private LocalDateTime postingTime;

    /**
     * 帖子状态.枚举
     * {@link PostStatus}
     */
    @JSONField(ordinal = 60)
    private String postStatus;
}
