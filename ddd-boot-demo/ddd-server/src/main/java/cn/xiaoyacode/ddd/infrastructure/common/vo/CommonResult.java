package cn.xiaoyacode.ddd.infrastructure.common.vo;

import cn.xiaoyacode.ddd.infrastructure.common.exception.ErrorCode;
import cn.xiaoyacode.ddd.infrastructure.common.exception.GlobalException;
import cn.xiaoyacode.ddd.infrastructure.common.exception.ServiceException;
import cn.xiaoyacode.ddd.infrastructure.common.exception.enums.GlobalErrorCodeConstants;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.util.Assert;

import java.io.Serializable;

/**
 * 公共返回
 *
 * @author xieya
 * @link 设计参见 https://github.com/YunaiV/onemall
 * @date 2021/7/13
 */
@Getter
@Setter
@ToString
public class CommonResult<T> implements Serializable {
    private static final long serialVersionUID = 2700947483087321836L;
    /**
     * 错误码
     *
     * @see ErrorCode#getCode()
     */
    @JSONField(ordinal = 1)
    private Integer code;

    /**
     * 错误提示，用户可阅读
     *
     * @see ErrorCode#getMessage()
     */
    @JSONField(ordinal = 5)
    private String message;
    /**
     * 错误明细，内部调试错误
     */
    @JSONField(ordinal = 20)
    private String detailMessage;

    /**
     * 返回数据，可接受复杂构造
     */
    @JSONField(ordinal = 10)
    private T data;

    /**
     * 将传入的 result 对象，转换成另外一个泛型结果的对象
     * <p>
     * 因为 A 方法返回的 CommonResult 对象，不满足调用其的 B 方法的返回，所以需要进行转换。
     *
     * @param result 传入的 result 对象
     * @param <T>    返回的泛型
     * @return 新的 CommonResult 对象
     */
    public static <T> CommonResult<T> error(CommonResult<?> result) {
        return error(result.getCode(), result.getMessage(), result.detailMessage);
    }

    public static <T> CommonResult<T> error(Integer code, String message) {
        return error(code, message, null);
    }

    public static <T> CommonResult<T> error(Integer code, String message, String detailMessage) {
        Assert.isTrue(!GlobalErrorCodeConstants.SUCCESS.getCode().equals(code), "code 必须是错误的！");
        CommonResult<T> result = new CommonResult<>();
        result.code = code;
        result.message = message;
        result.detailMessage = detailMessage;
        return result;
    }

    public static <T> CommonResult<T> success(T data) {
        CommonResult<T> result = new CommonResult<>();
        result.code = GlobalErrorCodeConstants.SUCCESS.getCode();
        result.data = data;
        result.message = "";
        return result;
    }

    public static <T> CommonResult<T> error(ServiceException serviceException) {
        return error(serviceException.getCode(), serviceException.getMessage(),
                serviceException.getDetailMessage());
    }

    public static <T> CommonResult<T> error(GlobalException globalException) {
        return error(globalException.getCode(), globalException.getMessage(),
                globalException.getDetailMessage());
    }

    public CommonResult<T> setDetailMessage(String detailMessage) {
        this.detailMessage = detailMessage;
        return this;
    }


    // ========= 和 Exception 异常体系集成 =========

    /**
     * 避免序列化
     *
     * @return
     */
    @JSONField(serialize = false)
    public boolean isSuccess() {
        return GlobalErrorCodeConstants.SUCCESS.getCode().equals(code);
    }

    @JSONField(serialize = false)
    public boolean isError() {
        return !isSuccess();
    }

    /**
     * 判断是否有异常。如果有，则抛出 {@link GlobalException} 或 {@link ServiceException} 异常
     */
    public void checkError() throws GlobalException, ServiceException {
        if (isSuccess()) {
            return;
        }
        // 全局异常
        if (GlobalErrorCodeConstants.isMatch(code)) {
            throw new GlobalException(code, message).setDetailMessage(detailMessage);
        }
        // 业务异常
        throw new ServiceException(code, message).setDetailMessage(detailMessage);
    }

}
