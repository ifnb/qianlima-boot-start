package cn.xiaoyacode.ddd.infrastructure.common.domain;

import java.io.Serializable;

/**
 * 领域值对象
 *
 * @author xieya
 * @date 2021/7/19
 */
public interface ValueObject<T> extends Serializable {
    /**
     * 值对象通过其属性的值进行比较，它们没有身份。
     *
     * @param other 另一个值对象
     * @return 如果给定的值对象和这个值对象的属性相同，返回true
     */
    boolean sameValueAs(T other);
}
