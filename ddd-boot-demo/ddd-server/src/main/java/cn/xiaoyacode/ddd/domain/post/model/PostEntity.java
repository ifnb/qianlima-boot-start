package cn.xiaoyacode.ddd.domain.post.model;

import cn.xiaoyacode.ddd.domain.post.model.vo.PostAuthorId;
import cn.xiaoyacode.ddd.domain.post.model.vo.PostContent;
import cn.xiaoyacode.ddd.domain.post.model.vo.PostTitle;
import cn.xiaoyacode.ddd.infrastructure.common.domain.Entity;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * 帖子实体
 *
 * @author xieya
 * @date 2021/7/13
 */
@Data
@Accessors(chain = true)
public class PostEntity implements Entity<PostEntity> {
    /**
     * 帖子id
     */
    private Long postId;

    /**
     * 帖子标题
     */
    private PostTitle title;

    /**
     * 帖子源内容
     */
    private PostContent sourceContent;

    /**
     * 发帖时间
     */
    private LocalDateTime postingTime;

    /**
     * 帖子状态.枚举
     * {@link PostStatus}
     */
    private PostStatus postStatus;

    /**
     * 帖子作者Id
     * {@link PostAuthorId}
     */
    private PostAuthorId authorId;

    @Override
    public boolean sameIdentityAs(PostEntity other) {
        return other != null && other.equals(this);
    }
}
