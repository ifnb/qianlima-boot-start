package cn.xiaoyacode.ddd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * 开启异步
 *
 * @author xieya
 */
@SpringBootApplication
@EnableAsync
public class DddBootDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DddBootDemoApplication.class, args);
    }

}
