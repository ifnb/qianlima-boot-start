package cn.xiaoyacode.ddd.domain.post.model.vo;

import cn.xiaoyacode.ddd.infrastructure.common.domain.ValueObject;
import cn.xiaoyacode.ddd.infrastructure.common.exception.util.ServiceExceptionUtil;
import cn.xiaoyacode.ddd.infrastructure.constants.post.PostErrorCodeConstants;
import lombok.Data;

import java.util.Objects;

/**
 * 帖子作者
 *
 * @author xieya
 * @date 2021/7/13
 */
@Data
public class PostAuthorId implements ValueObject<PostAuthorId> {

    private final Long authorId;

    public PostAuthorId(final Long authorId) {
        // 异常校验
        ServiceExceptionUtil.must(Objects.isNull(authorId), PostErrorCodeConstants.POST_AUTHOR_ID_IS_NULL);
        this.authorId = authorId;
    }

    @Override
    public boolean sameValueAs(PostAuthorId other) {
        return other != null && this.authorId.equals(other.authorId);
    }
}
