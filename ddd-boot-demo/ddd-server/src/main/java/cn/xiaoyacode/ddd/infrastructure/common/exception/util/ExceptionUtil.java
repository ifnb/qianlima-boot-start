package cn.xiaoyacode.ddd.infrastructure.common.exception.util;


import org.apache.commons.lang3.exception.ExceptionUtils;

/**
 * 异常工具
 *
 * @author xieya
 * @date 2021/7/15
 */
public class ExceptionUtil {
    public static String getMessage(Throwable th) {
        // apache lang3的工具类，需要导入commons.lang3依赖包
        return ExceptionUtils.getMessage(th);
    }

    public static String getRootCauseMessage(Throwable th) {
        return ExceptionUtils.getRootCauseMessage(th);
    }

    public static String getStackTrace(Throwable th) {
        return ExceptionUtils.getStackTrace(th);
    }
}
