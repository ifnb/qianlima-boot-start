package cn.xiaoyacode.ddd.application.user.service;

import cn.xiaoyacode.ddd.infrastructure.common.vo.CommonResult;
import cn.xiaoyacode.ddd.ui.dto.user.req.UserCreateReqDTO;

/**
 * @author xieya
 * @date 2021/7/16
 */
public interface UserApplicationService {

    /**
     * 创建用户
     *
     * @param userCreateReqDTO 用户创建请求参数
     * @return
     */
    CommonResult<Long> createUser(UserCreateReqDTO userCreateReqDTO);
}
