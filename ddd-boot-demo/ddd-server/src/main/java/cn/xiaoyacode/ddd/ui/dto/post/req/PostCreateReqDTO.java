package cn.xiaoyacode.ddd.ui.dto.post.req;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author xieya
 * @date 2021/7/13
 */
@Data
@Accessors(chain = true)
public class PostCreateReqDTO {

    @NotNull(message = "用户ID不能为空")
    private Long userId;

    @NotBlank(message = "文章标题不能为空")
    private String title;

    @NotBlank(message = "文章内容不能为空")
    private String sourceContent;

}
