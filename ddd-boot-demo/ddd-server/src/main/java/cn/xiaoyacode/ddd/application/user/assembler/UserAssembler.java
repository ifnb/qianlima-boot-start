package cn.xiaoyacode.ddd.application.user.assembler;

import cn.xiaoyacode.ddd.domain.user.model.UserEntity;
import cn.xiaoyacode.ddd.ui.dto.user.req.UserCreateReqDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * @author xieya
 * @date 2021/7/20
 */
@Mapper
public interface UserAssembler {
    UserAssembler INSTANCE = Mappers.getMapper(UserAssembler.class);

    default UserEntity convertUserCreate2entiry(UserCreateReqDTO userCreateReqDTO) {
        return new UserEntity();
    }
}
