package cn.xiaoyacode.ddd.domain.post.model.vo;

import cn.xiaoyacode.ddd.infrastructure.common.domain.ValueObject;
import cn.xiaoyacode.ddd.infrastructure.common.exception.util.ServiceExceptionUtil;
import cn.xiaoyacode.ddd.infrastructure.constants.post.PostConstants;
import cn.xiaoyacode.ddd.infrastructure.constants.post.PostErrorCodeConstants;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

/**
 * 帖子内容
 *
 * @author xieya
 * @date 2021/7/19
 */
@Data
public class PostContent implements ValueObject<PostContent> {
    private final String content;

    public PostContent(String content) {
        // 文章内容不少于16个字
        boolean flag = StringUtils.isBlank(content) || content.length() < PostConstants.POST_SOURCE_CONTENT_MIN_LENGTH;
        ServiceExceptionUtil.must(flag, PostErrorCodeConstants.POST_CONTENT_OVER_LIMIT);
        this.content = content;
    }

    @Override
    public boolean sameValueAs(PostContent other) {
        return other != null && other.content.equals(this.content);
    }
}
