package cn.xiaoyacode.ddd.infrastructure.converter.user;

import cn.xiaoyacode.ddd.domain.user.model.UserEntity;
import cn.xiaoyacode.ddd.domain.user.model.enums.DeleteFlagEnum;
import cn.xiaoyacode.ddd.domain.user.model.enums.UserStatusEnum;
import cn.xiaoyacode.ddd.domain.user.model.vo.Email;
import cn.xiaoyacode.ddd.domain.user.model.vo.UserId;
import cn.xiaoyacode.ddd.domain.user.model.vo.UserName;
import cn.xiaoyacode.ddd.infrastructure.persistence.mysql.po.user.UserPO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

/**
 * @author xieya
 * @date 2021/7/16
 */
@Mapper
public interface UserConvert {

    UserConvert INSTANCE = Mappers.getMapper(UserConvert.class);

    /**
     * bo -> po
     *
     * @param user 用户创建参数
     * @return po
     */
    @Mappings({
            @Mapping(source = "username", target = "name"),
            @Mapping(source = "age", target = "age"),
    })
    UserPO covert(UserEntity user);

    // ==============attr convert start

    default String attrConvert(UserName userName) {
        return userName.getUsername();
    }

    default String attrConvert(Email email) {
        return email.getEmail();
    }

    default Long attrConvert(UserId userId) {
        return userId.getUserId();
    }

    default Integer attrConvert(DeleteFlagEnum deleteFlagEnum) {
        return deleteFlagEnum.getCode();
    }

    default Integer attrConvert(UserStatusEnum userStatusEnum) {
        return userStatusEnum.getCode();
    }

    // ==============attr convert end

}
