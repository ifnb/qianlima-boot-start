/*
 Navicat Premium Data Transfer

 Source Server         : 本地连接
 Source Server Type    : MySQL
 Source Server Version : 50727
 Source Host           : localhost:3306
 Source Schema         : ddd-boot

 Target Server Type    : MySQL
 Target Server Version : 50727
 File Encoding         : 65001

 Date: 16/07/2021 16:34:51
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户名',
  `age` int(3) NOT NULL COMMENT '年龄',
  `email` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '邮箱',
  `status` tinyint(4) NOT NULL COMMENT '用户状态，1,启用, 2,禁用',
  `deleted` bit(1) NOT NULL COMMENT '是否删除, 0,否，1是',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'lili', 20, '123456@qq.com', 1, b'0', '2021-07-16 10:40:13', '2021-07-16 10:40:13');
INSERT INTO `user` VALUES (2, 'admin', 10, '123456789@qq.com', 1, b'0', '2021-07-16 10:44:51', '2021-07-16 10:44:51');
INSERT INTO `user` VALUES (3, 'test', 12, '123456789@qq.com', 1, b'0', '2021-07-16 11:38:53', '2021-07-16 11:38:53');
INSERT INTO `user` VALUES (4, '阿尔法', 99, 'teset123@qq.com', 1, b'0', '2021-07-16 16:03:31', '2021-07-16 16:03:31');
INSERT INTO `user` VALUES (5, '阿尔法', 99, 'teset123@qq.com', 1, b'0', '2021-07-16 16:03:39', '2021-07-16 16:03:39');
INSERT INTO `user` VALUES (6, '阿尔法2', 99, 'teset123@qq.com', 1, b'0', '2021-07-16 16:27:23', '2021-07-16 16:27:23');

SET FOREIGN_KEY_CHECKS = 1;
